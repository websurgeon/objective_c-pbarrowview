//
//  PBArrowViewTestCase.h
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

@class PBArrowView;

@interface PBArrowViewTestCase : XCTestCase {
    PBArrowView *sut;
    CGRect rect1;
}

@end