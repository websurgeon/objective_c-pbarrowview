//
//  PBArrowViewInitializerTests.m
//  PBArrowViewTests
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import "PBArrowView.h"

#import "PBArrowViewTestCase.h"

@interface PBArrowViewInitializerTests : PBArrowViewTestCase

@end

@implementation PBArrowViewInitializerTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

// --------------------------------------------------------
// Designated Initializer
#pragma mark - initWithFrame:rotation:arrowLength:headWidth:headLength:tailWidth:tailLength:strokeColor:strokeWidth:fillColor

- (void)testPBArrowView_initWithFrameAndAllParams_setsDefaults
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThat(sut, is(notNilValue()));
    [self assertArrowDrawingDefaultsSet];
}

- (void)testPBArrowView_initWithFrameAndAllParams_usesSuppliedFrame
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThatBool(CGRectEqualToRect([sut frame], rect1),  is(equalToBool(YES)));
}

- (void)testPBArrowView_initWithFrameAndAllParams_setsRotation
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:M_PI arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThatFloat(sut.rotation, is(equalToFloat(M_PI)));
}

- (void)testPBArrowView_initWithFrameAndAllParams_setsArrowDimensions
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:1.2 headWidth:2.3 headLength:3.4 tailWidth:4.5 tailLength:5.6 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThatFloat(sut.arrowLength, is(equalToFloat(1.2)));
    assertThatFloat(sut.headWidth, is(equalToFloat(2.3)));
    assertThatFloat(sut.headLength, is(equalToFloat(3.4)));
    assertThatFloat(sut.tailWidth, is(equalToFloat(4.5)));
    assertThatFloat(sut.tailLength, is(equalToFloat(5.6)));
}

- (void)testPBArrowView_initWithFrameAndAllParams_setsDrawingProps
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:[UIColor redColor] strokeWidth:1.2 fillColor:[UIColor blueColor]];
    
    assertThat(sut.strokeColor, is(equalTo([UIColor redColor])));
    assertThatFloat(sut.strokeWidth, is(equalToFloat(1.2)));
    assertThat(sut.fillColor, is(equalTo([UIColor blueColor])));
}

- (void)testPBArrowView_initWithFrameAndAllParams_setsClearBackgroundColor
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThat(sut.backgroundColor, is(equalTo([UIColor clearColor])));
}

- (void)testPBArrowView_initWithFrameAndAllParams_setsViewNotOpaque
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0 strokeColor:nil strokeWidth:0 fillColor:nil];
    
    assertThatBool(sut.opaque, is(equalToBool(NO)));
}

// --------------------------------------------------------
#pragma mark - initWithFrame:rotation:arrowLength:headWidth:headLength:tailWidth:tailLength:

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_setsDefaults
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0];
    
    assertThat(sut, is(notNilValue()));
    [self assertArrowDrawingDefaultsSet];
}

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_usesSuppliedFrame
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0];
    
    assertThatBool(CGRectEqualToRect([sut frame], rect1),  is(equalToBool(YES)));
}

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_setsRotation
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:M_PI arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0];
    
    assertThatFloat(sut.rotation, is(equalToFloat(M_PI)));
}

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_setsArrowDimensions
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:1.2 headWidth:2.3 headLength:3.4 tailWidth:4.5 tailLength:5.6];
    
    assertThatFloat(sut.arrowLength, is(equalToFloat(1.2)));
    assertThatFloat(sut.headWidth, is(equalToFloat(2.3)));
    assertThatFloat(sut.headLength, is(equalToFloat(3.4)));
    assertThatFloat(sut.tailWidth, is(equalToFloat(4.5)));
    assertThatFloat(sut.tailLength, is(equalToFloat(5.6)));
}

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_setsClearBackgroundColor
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0];
    
    assertThat(sut.backgroundColor, is(equalTo([UIColor clearColor])));
}

- (void)testPBArrowView_initWithFrame_rotation_arrowDimensions_setsViewNotOpaque
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0 arrowLength:0 headWidth:0 headLength:0 tailWidth:0 tailLength:0];
    
    assertThatBool(sut.opaque, is(equalToBool(NO)));
}

// --------------------------------------------------------
#pragma mark - initWithFrame:rotation

- (void)testPBArrowView_initWithFrame_rotation_setsDefaults
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0];
    
    assertThat(sut, is(notNilValue()));
    [self assertArrowDimentionDefaultsSet];
    [self assertArrowDrawingDefaultsSet];
}

- (void)testPBArrowView_initWithFrame_rotation_usesSuppliedFrame
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:0];
    
    assertThatBool(CGRectEqualToRect([sut frame], rect1),  is(equalToBool(YES)));
}

- (void)testPBArrowView_initWithFrame_rotation_setsRotation
{
    sut = [[PBArrowView alloc] initWithFrame:rect1 rotation:M_PI];
    
    assertThatFloat(sut.rotation, is(equalToFloat(M_PI)));
}

- (void)testPBArrowView_initWithFrame_rotation_setsClearBackgroundColor
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0];
    
    assertThat(sut.backgroundColor, is(equalTo([UIColor clearColor])));
}

- (void)testPBArrowView_initWithFrame_rotation_setsViewNotOpaque
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero rotation:0];
    
    assertThatBool(sut.opaque, is(equalToBool(NO)));
}

// --------------------------------------------------------
#pragma mark - initWithFrame:

- (void)testPBArrowView_initWithFrame_setsDefaults
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero];
    
    assertThat(sut, is(notNilValue()));
    [self assertArrowDimentionDefaultsSet];
    [self assertArrowDrawingDefaultsSet];
}

- (void)testPBArrowView_initWithFrame_usesSuppliedFrame
{
    sut = [[PBArrowView alloc] initWithFrame:rect1];
    
    assertThatBool(CGRectEqualToRect([sut frame], rect1),  is(equalToBool(YES)));
}

- (void)testPBArrowView_initWithFrame_setsClearBackgroundColor
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero];
    
    assertThat(sut.backgroundColor, is(equalTo([UIColor clearColor])));
}

- (void)testPBArrowView_initWithFrame_setsViewNotOpaque
{
    sut = [[PBArrowView alloc] initWithFrame:CGRectZero];
    
    assertThatBool(sut.opaque, is(equalToBool(NO)));
}

// --------------------------------------------------------
#pragma mark - init

- (void)testPBArrowView_init_setsDefaults
{
    sut = [[PBArrowView alloc] init];
    
    assertThat(sut, is(notNilValue()));
    assertThatBool(CGRectEqualToRect([sut frame], CGRectZero),  is(equalToBool(YES)));
    assertThatFloat(sut.rotation, is(equalToFloat(PBARROWVIEW_DEFAULT_ROTATION)));
    [self assertArrowDimentionDefaultsSet];
    [self assertArrowDrawingDefaultsSet];
}

- (void)testPBArrowView_init_setsClearBackgroundColor
{
    sut = [[PBArrowView alloc] init];
    
    assertThat(sut.backgroundColor, is(equalTo([UIColor clearColor])));
}

- (void)testPBArrowView_init_setsViewNotOpaque
{
    sut = [[PBArrowView alloc] init];
    
    assertThatBool(sut.opaque, is(equalToBool(NO)));
}

// --------------------------------------------------------
#pragma mark - Helper Methods

- (void)assertArrowDimentionDefaultsSet
{
    assertThatFloat(sut.arrowLength, is(equalToFloat(PBARROWVIEW_DEFAULT_ARROW_LENGTH)));
    assertThatFloat(sut.headWidth, is(equalToFloat(PBARROWVIEW_DEFAULT_HEAD_WIDTH)));
    assertThatFloat(sut.headLength, is(equalToFloat(PBARROWVIEW_DEFAULT_HEAD_LENGTH)));
    assertThatFloat(sut.tailWidth, is(equalToFloat(PBARROWVIEW_DEFAULT_TAIL_WIDTH)));
    assertThatFloat(sut.tailLength, is(equalToFloat(PBARROWVIEW_DEFAULT_TAIL_LENGTH)));
}

- (void)assertArrowDrawingDefaultsSet
{
    assertThat(sut.strokeColor, is(equalTo(PBARROWVIEW_DEFAULT_STROKE_COLOR)));
    assertThatFloat(sut.strokeWidth, is(equalToFloat(PBARROWVIEW_DEFAULT_STROKE_WIDTH)));
    assertThat(sut.fillColor, is(equalTo(PBARROWVIEW_DEFAULT_FILL_COLOR)));
}

@end
