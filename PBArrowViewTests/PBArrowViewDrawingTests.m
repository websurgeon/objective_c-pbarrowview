//
//  PBArrowViewDrawingTests.m
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import "PBArrowView.h"

#import "PBArrowViewTestCase.h"

@interface PBArrowViewDrawingTests : PBArrowViewTestCase

@end

@implementation PBArrowViewDrawingTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testArrowToFitSizeDrawsArrowWithCorrectPoints
{
//        (1)
//         /\
//       /    \
//     /        \
//   /            \
// (7)----    ----(2)
//     (6)|  |(3)
//        |  |
//     (5)|__|(4)
//

    CGSize size = CGSizeMake(100, 100);
    UIBezierPath *arrowPath = [PBArrowView arrowToFitSize:size rotation:0 arrowLength:1 headWidth:1 headLength:0.5 tailWidth:0.25 tailLength:0.5];
//  (1)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width / 2.0f, 0)], is(equalToBool(YES)));
//  (2)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width, size.height / 2.0f)], is(equalToBool(YES)));
//  (3)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width - (size.width * ((1.0f - 0.25f) / 2.0f)), size.height / 2.0f)], is(equalToBool(YES)));
//  (4)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width - (size.width * ((1.0f - 0.25f) / 2.0f)), size.height)], is(equalToBool(YES)));
//  (5)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width * ((1.0f - 0.25f) / 2.0f), size.height)], is(equalToBool(YES)));
//  (6)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width * ((1.0f - 0.25f) / 2.0f), size.height / 2.0f)], is(equalToBool(YES)));
//  (7)
    assertThatBool([arrowPath containsPoint:CGPointMake(0, size.height / 2.0f)], is(equalToBool(YES)));
}

- (void)testArrowToFitSizeDrawsRotatedArrowWithCorrectPoints
{
//  Rotated 2 x PI (180 degrees)
//
//        (4)|--|(5)
//           |  |
//        (3)|  |(6)
//    (2)----    ----(7)
//      \             /
//        \         /
//          \     /
//            \ /
//            (1)
//
    
    CGSize size = CGSizeMake(100, 100);
    UIBezierPath *arrowPath = [PBArrowView arrowToFitSize:size rotation:M_PI arrowLength:1 headWidth:1 headLength:0.5 tailWidth:0.25 tailLength:0.5];

//  (1)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width / 2.0f, size.height)], is(equalToBool(YES)));
//  (2)
    assertThatBool([arrowPath containsPoint:CGPointMake(0, size.height / 2.0f)], is(equalToBool(YES)));
//  (3)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width * ((1.0f - 0.25f) / 2.0f), size.height / 2.0f)], is(equalToBool(YES)));
//  (4)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width * ((1.0f - 0.25f) / 2.0f), 0)], is(equalToBool(YES)));
//  (5)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width - (size.width * ((1.0f - 0.25f) / 2.0f)), 0)], is(equalToBool(YES)));
//  (6)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width - (size.width * ((1.0f - 0.25f) / 2.0f)), size.height / 2.0f)], is(equalToBool(YES)));
//  (7)
    assertThatBool([arrowPath containsPoint:CGPointMake(size.width, size.height / 2.0f)], is(equalToBool(YES)));

}

@end
