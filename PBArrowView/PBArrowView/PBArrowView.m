//
//  PBArrowView.m
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import "PBArrowView.h"

@implementation PBArrowView

// designated initializer
- (id)initWithFrame:(CGRect)aRect
           rotation:(float)rotation
        arrowLength:(float)arrowLength
          headWidth:(float)headWidth
         headLength:(float)headLength
          tailWidth:(float)tailWidth
         tailLength:(float)tailLength
        strokeColor:(UIColor *)strokeColor
        strokeWidth:(float)strokeWidth
          fillColor:(UIColor *)fillColor
{
    self = [super initWithFrame:aRect];
    if (self) {
        [self setup];
        self.rotation = rotation;
        self.arrowLength = arrowLength;
        self.headWidth = headWidth;
        self.headLength = headLength;
        self.tailWidth = tailWidth;
        self.tailLength = tailLength;
        self.strokeColor = strokeColor ?: PBARROWVIEW_DEFAULT_STROKE_COLOR;
        self.strokeWidth = strokeWidth;
        self.fillColor = fillColor ?: PBARROWVIEW_DEFAULT_FILL_COLOR;
    }
    return self;
    
}

- (id)initWithFrame:(CGRect)aRect
           rotation:(float)rotation
        arrowLength:(float)arrowLength
          headWidth:(float)headWidth
         headLength:(float)headLength
          tailWidth:(float)tailWidth
         tailLength:(float)tailLength
{
    self = [self initWithFrame:aRect
                      rotation:rotation
                   arrowLength:arrowLength
                     headWidth:headWidth
                    headLength:headLength
                     tailWidth:tailWidth
                    tailLength:tailLength
                   strokeColor:PBARROWVIEW_DEFAULT_STROKE_COLOR
                   strokeWidth:PBARROWVIEW_DEFAULT_STROKE_WIDTH
                     fillColor:PBARROWVIEW_DEFAULT_FILL_COLOR];
    return self;
}

- (id)initWithFrame:(CGRect)aRect rotation:(float)rotation
{
    self = [self initWithFrame:aRect
                      rotation:rotation
                   arrowLength:PBARROWVIEW_DEFAULT_ARROW_LENGTH
                     headWidth:PBARROWVIEW_DEFAULT_HEAD_WIDTH
                    headLength:PBARROWVIEW_DEFAULT_HEAD_LENGTH
                     tailWidth:PBARROWVIEW_DEFAULT_TAIL_WIDTH
                    tailLength:PBARROWVIEW_DEFAULT_TAIL_LENGTH];
    return self;
}

- (id)initWithFrame:(CGRect)aRect
{
    self = [self initWithFrame:aRect rotation:PBARROWVIEW_DEFAULT_ROTATION];
    return self;
}

- (id)init
{
    self = [self initWithFrame:CGRectZero rotation:PBARROWVIEW_DEFAULT_ROTATION];
    return self;
}

- (void)setup
{
    [self setBackgroundColor:[UIColor clearColor]];
    [self setOpaque:NO];
}

- (void)drawRect:(CGRect)rect
{
    [self.fillColor setFill];
    [self.strokeColor setStroke];
    
    UIBezierPath *arrowPath = [PBArrowView arrowToFitSize:rect.size rotation:self.rotation arrowLength:self.arrowLength headWidth:self.headWidth headLength:self.headLength tailWidth:self.tailWidth tailLength:self.tailLength];
    
    [arrowPath setLineWidth:self.strokeWidth];
    
    if (self.fillColor) [arrowPath fill];
    if (self.strokeColor) [arrowPath stroke];
}


+ (UIBezierPath *)arrowToFitSize:(CGSize)size rotation:(float)rotation arrowLength:(float)arrowLength headWidth:(float)headWidth headLength:(float)headLength tailWidth:(float)tailWidth tailLength:(float)tailLength
{
    NSArray *arrowPoints = [PBArrowView pointsForArrowWithLength:arrowLength headWidth:headWidth headLength:headLength tailWidth:tailWidth tailLength:tailLength];
    
    UIBezierPath *arrowPath = [PBArrowView pathFromArrayOfPoints:arrowPoints];
    
    CGPoint arrowCenter = CGPointMake(headWidth/2.0f, arrowLength/2.0f);
    
    // rotate arrow path about center point if required
    if (fmodf(rotation, 2.0f * M_PI) != 0) arrowPath = [PBArrowView path:arrowPath rotated:rotation aboutPoint:arrowCenter];
    
    [PBArrowView scalePath:arrowPath toFitSize:size];
    
    return arrowPath;
}

+ (NSArray *)pointsForArrowWithLength:(float)arrowLength headWidth:(float)headWidth headLength:(float)headLength tailWidth:(float)tailWidth tailLength:(float)tailLength
{
    // convert all arrow dimentions to unit values relative to arrowLength
    headWidth = headWidth / arrowLength;
    headLength = headLength / arrowLength;
    tailWidth = tailWidth / arrowLength;
    tailLength = tailLength / arrowLength;
    arrowLength = 1.0f;
    
    
    // calculate main arrow dimentions to use
    
    float halfHeadWidth = headWidth / 2.0f;
    float halfTailWidth = tailWidth / 2.0f;
    float tailTop = arrowLength - tailLength;
    
    CGPoint startPoint = CGPointMake(headWidth/2.0f, 0);
    
    // generate points that describe the arrow
    CGPoint point1 = CGPointMake(startPoint.x + halfHeadWidth, startPoint.y + headLength);
    CGPoint point2 = CGPointMake(startPoint.x + halfTailWidth, startPoint.y + tailTop);
    CGPoint point3 = CGPointMake(startPoint.x + halfTailWidth, startPoint.y + arrowLength);
    CGPoint point4 = CGPointMake(startPoint.x - halfTailWidth, startPoint.y + arrowLength);
    CGPoint point5 = CGPointMake(startPoint.x - halfTailWidth, startPoint.y + tailTop);
    CGPoint point6 = CGPointMake(startPoint.x - halfHeadWidth, startPoint.y + headLength);
    
    return @[
             [NSValue valueWithCGPoint:startPoint],
             [NSValue valueWithCGPoint:point1],
             [NSValue valueWithCGPoint:point2],
             [NSValue valueWithCGPoint:point3],
             [NSValue valueWithCGPoint:point4],
             [NSValue valueWithCGPoint:point5],
             [NSValue valueWithCGPoint:point6]
             ];
}

+ (UIBezierPath *)pathFromArrayOfPoints:(NSArray *)array
{
    NSMutableArray *pointsArray = [array mutableCopy];
    
    UIBezierPath *arrowPath = [UIBezierPath bezierPath];

    // create path from arrow points
    NSValue *startPointValue = [pointsArray firstObject];
    [arrowPath moveToPoint:[startPointValue CGPointValue]];
    [pointsArray removeObjectAtIndex:0];
    for (NSValue *pointValue in pointsArray) {
        [arrowPath addLineToPoint:[pointValue CGPointValue]];
    }
    [arrowPath closePath];
    return arrowPath;
}

+ (UIBezierPath *)path:(UIBezierPath *)path rotated:(float)rotation aboutPoint:(CGPoint)fixedPoint
{
    UIBezierPath *copyOfPath = [path copy];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(fixedPoint.x, fixedPoint.y);
    transform = CGAffineTransformRotate(transform, rotation);
    transform = CGAffineTransformTranslate(transform,-fixedPoint.x,-fixedPoint.y);
    [copyOfPath applyTransform:transform];
    
    return copyOfPath;
}

+ (void)scalePath:(UIBezierPath *)path toFitSize:(CGSize)size
{
    CGRect pathBounds = [path bounds];
    
    // calculate scale to fit arrow within supplied size
    float scaleX = size.width / pathBounds.size.width;
    float scaleY = size.height / pathBounds.size.height;
    
    // minimum scale factor should be used to ensure that completely fits into supplied size
    float scale = MIN(scaleX, scaleY);
    
    // transform to scale arrow to fit supplied size
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    
    [path applyTransform:transform];
    
    // move scaled arrow to center of size dimensions
    [PBArrowView positionPath:path inCenterOfRect:CGRectMake(0,0,size.width, size.height)];
}

+ (void)positionPath:(UIBezierPath *)path inCenterOfRect:(CGRect)rect
{
    CGRect pathBounds = [path bounds];
    
    // move path to rect origin
    CGAffineTransform transform = CGAffineTransformMakeTranslation(-pathBounds.origin.x + rect.origin.x,-pathBounds.origin.y + rect.origin.y);
    [path applyTransform:transform];
    
    // move path to center of rect
    transform =  CGAffineTransformMakeTranslation((rect.size.width -  pathBounds.size.width)/2.0f, (rect.size.height -  pathBounds.size.height)/2.0f);
    [path applyTransform:transform];
}

// return this view as UIImage
- (UIImage *)image
{
    UIView *view = self;
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, self.opaque, [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
