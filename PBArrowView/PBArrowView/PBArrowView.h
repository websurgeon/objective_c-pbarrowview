//
//  PBArrowView.h
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//
//
//                       headWidth
//                    |-------------|
//              ---          ..         ---
//               |          ....         |
//               |         ......        |
//               |        ........       |  headLength
//   arrowLength |       ..........      |     ___
//               |      ..  ....  ..     |      |
//               |     .    ....    .   ___     |
//               |          ....                |  tailLength
//               |          ....                |
//               |          ....                |
//              ___         ....               ---
//
//                          |--|  tailWidth
//
//
//

#import <UIKit/UIKit.h>

#define PBARROWVIEW_DEFAULT_ROTATION 0
#define PBARROWVIEW_DEFAULT_ARROW_LENGTH 1.0f
#define PBARROWVIEW_DEFAULT_HEAD_WIDTH 0.5f
#define PBARROWVIEW_DEFAULT_HEAD_LENGTH 0.55f
#define PBARROWVIEW_DEFAULT_TAIL_LENGTH 0.55f
#define PBARROWVIEW_DEFAULT_TAIL_WIDTH 0.125f
#define PBARROWVIEW_DEFAULT_STROKE_COLOR nil
#define PBARROWVIEW_DEFAULT_STROKE_WIDTH 0
#define PBARROWVIEW_DEFAULT_FILL_COLOR [UIColor grayColor]

@interface PBArrowView : UIView

@property (nonatomic) float rotation;
@property (nonatomic) float arrowLength;
@property (nonatomic) float headWidth;
@property (nonatomic) float headLength;
@property (nonatomic) float tailLength;
@property (nonatomic) float tailWidth;
@property (nonatomic, copy) UIColor *fillColor;
@property (nonatomic, copy) UIColor *strokeColor;
@property (nonatomic) float strokeWidth;

// designated initializer
- (id)initWithFrame:(CGRect)aRect
           rotation:(float)rotation
        arrowLength:(float)arrowLength
          headWidth:(float)headWidth
         headLength:(float)headLength
          tailWidth:(float)tailWidth
         tailLength:(float)tailLength
        strokeColor:(UIColor *)strokeColor
        strokeWidth:(float)strokeWidth
          fillColor:(UIColor *)fillColor;

- (id)initWithFrame:(CGRect)aRect
           rotation:(float)rotation
        arrowLength:(float)arrowLength
          headWidth:(float)headWidth
         headLength:(float)headLength
          tailWidth:(float)tailWidth
         tailLength:(float)tailLength;

- (id)initWithFrame:(CGRect)aRect
           rotation:(float)rotation;


+ (UIBezierPath *)arrowToFitSize:(CGSize)size rotation:(float)rotation arrowLength:(float)arrowLength headWidth:(float)headWidth headLength:(float)headLength tailWidth:(float)tailWidth tailLength:(float)tailLength;

- (UIImage *)image;
;
@end
