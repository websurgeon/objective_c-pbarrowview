//
//  ViewController.m
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import "ViewController.h"

#import "PBArrowView.h"

#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self.view addSubview:[self arrow1]];
    [self.view addSubview:[self arrow2]];
    
    PBArrowView *arrow3 = [self arrow3];
    [self.view addSubview:arrow3];

    UIImageView *imgView = [[UIImageView alloc] initWithImage:[arrow3 image]];
    [imgView setFrame:CGRectMake(arrow3.frame.origin.x + arrow3.frame.size.width + 10, 40, arrow3.frame.size.width, arrow3.frame.size.height)];
    [self.view addSubview:imgView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PBArrowView *)arrow1
{
    PBArrowView *view = [[PBArrowView alloc] initWithFrame:CGRectMake(20, 40, 10, 50)];
    [self setBorderOnView:view];
    return view;
}

- (PBArrowView *)arrow2
{
    PBArrowView *view = [[PBArrowView alloc] initWithFrame:CGRectMake(40, 40, 20, 50) rotation:M_PI*0.25];
    [self setBorderOnView:view];
    return view;
}

- (PBArrowView *)arrow3
{
    PBArrowView *view = [[PBArrowView alloc] initWithFrame:CGRectMake(70, 40, 75, 75) rotation:-M_PI*0.25];
    [self setBorderOnView:view];
    return view;
}

- (void)setBorderOnView:(UIView *)view
{
    view.layer.borderColor = [UIColor blueColor].CGColor;
    view.layer.borderWidth = 1.0f;
}

@end
