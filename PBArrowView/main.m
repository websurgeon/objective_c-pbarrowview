//
//  main.m
//  PBArrowView
//
//  Created by Peter on 01/03/2014.
//  Copyright (c) 2014 Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
